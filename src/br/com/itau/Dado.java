package br.com.itau;

import java.util.ArrayList;
import java.util.Random;

public class Dado {
   private int numero;

    public Dado() {
        Random aleatorio = new Random();
        this.numero = aleatorio.nextInt(6) + 1;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
