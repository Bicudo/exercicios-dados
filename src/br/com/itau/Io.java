package br.com.itau;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Io {

    public static Map<String, Integer> entradaValores () {
        Scanner scanner= new Scanner(System.in);
        System.out.println("Iniciando jogo de dados");
        System.out.println("Digite a quantidade de números:");
        Integer qtdNumeros = scanner.nextInt();

        System.out.println("Digite a quantidade de grupos:");
        Integer qtdGrupos = scanner.nextInt();

        Map<String, Integer> jogo = new HashMap<>();

        jogo.put("qtdNumeros", qtdNumeros);
        jogo.put("qtdGrupos", qtdGrupos);

        return jogo;
    }

    public static void imprime (ArrayList<Integer> numerosSorteados, int soma, int qtdNumeros) {

         if(qtdNumeros>1){
             for (Integer numeroSorteado : numerosSorteados) {
                    System.out.print(numeroSorteado + ",");
             }
             System.out.println(soma);
         }
         else{
            for(Integer numeroSorteado : numerosSorteados){
                System.out.println(numeroSorteado);
            }
         }
    }

}
