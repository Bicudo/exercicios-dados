package br.com.itau;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Sorteador {

    public Sorteador() {
    }

    public static ArrayList<Integer> sorteiaNumeros (int qtdNumeros, Dado dado) {
        ArrayList<Integer> numeros = new ArrayList<>();

        for (int i = 0; i < qtdNumeros; i++) {
            int valor = dado.getNumero();
            numeros.add(valor);
        }

        return numeros;
    }

    public static int somaDados (ArrayList<Integer> numerosSorteados){
        int soma=0;
        for (int i=0; i < numerosSorteados.size(); i++){
            soma = soma + numerosSorteados.get(i);
        }
        return soma;
    }

    public static void iniciaJogo() {
        Map<String, Integer> valores = Io.entradaValores();
        int qtdGrupos = valores.get("qtdGrupos");
        int qtdNumeros= valores.get("qtdNumeros");
        ArrayList numerosSorteados;

     if(qtdGrupos>1) {
         for (int i = 0; i < qtdGrupos; i++) {
             Dado dado = new Dado();
             numerosSorteados = Sorteador.sorteiaNumeros(qtdNumeros, dado);
             int soma = Sorteador.somaDados(numerosSorteados);
             Io.imprime(numerosSorteados, soma, qtdNumeros);
         }
     }
     else if(qtdGrupos == 1 && qtdNumeros >0){
            Dado dado = new Dado();
            numerosSorteados = Sorteador.sorteiaNumeros(qtdNumeros, dado);
            int soma= Sorteador.somaDados(numerosSorteados);
            Io.imprime(numerosSorteados, soma, qtdNumeros);
     }
    }

}

